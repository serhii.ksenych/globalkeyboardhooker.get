Examples of usage.

1. Handle which key was pressed.

private void HandleKey()
{
    GlobalKeyboardHook hook = new GlobalKeyboardHook();
    hook.KeyboardPressed += Hook_KeyboardPressed;
}

private void Hook_KeyboardPressed(object sender, GlobalKeyboardHookEventArgs e)
{
    var code = e.KeyboardData.VirtualCode;
    if (code == (int)VirtualKeysCodes.KEY_Z)
    {
        //some action
    }
}

2. Hadle pressed key state.

private void HandleKey()
{
    GlobalKeyboardHook hook = new GlobalKeyboardHook();
    hook.KeyboardPressed += Hook_KeyboardPressed;
}

private void Hook_KeyboardPressed(object sender, GlobalKeyboardHookEventArgs e)
{
    var code = e.KeyboardData.VirtualCode;
    if (e.KeyboardState == GlobalKeyboardHook.KeyboardState.KeyUp)
    {
        //some action
    }
}